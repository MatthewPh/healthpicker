document.getElementById('btn-submit').addEventListener('click', function(evt){
    evt.preventDefault();

    chrome.identity.getProfileUserInfo(function(info) { 

        var form = 
        {
            "user_id" : info.email,
            "age": document.getElementById('Age').value,
            "weight": document.getElementById('Weight').value,
            "height": document.getElementById('Height').value,
            "dietryRestrictions": {
                "diet_type":document.getElementById('Diet_type').value,
                "peanut": document.getElementById('Peanut').value
            }
        }

    var xmlhttp = new XMLHttpRequest();   // new HttpRequest instance 
    //var theUrl = "http://localhost:5000/send";
    var theUrl = 'https://healthpicker-uq8ke.transposit.io/api/v1/execute/setUserInfo?api_key=3ea38k2pe8mmpsm0b9qbidsm9v'
    xmlhttp.open("POST", theUrl);
    xmlhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    xmlhttp.send(JSON.stringify( {"parameters": form } ));

    })
    
})