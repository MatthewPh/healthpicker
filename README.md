# healthpicker

![storePhoto](https://gitlab.com/MatthewPh/healthpicker/raw/master/DevPostImage.PNG)

## Devpost Link
https://devpost.com/software/healthpicker-fw64cr

## What is it?
Our health chrome extension was created with the intent on making it more convenient when you shop online. It will help identify ingredients in food products and determine if it fits our users dietary needs. An example would be if our user was a Vegetarian. Our extension would scan in our meat keyword model, and then inform our user if there are any meat contents. We used Amazon.com as a main test demo.

## What we Accomplished
The team was able to complete a MVP our of health app. We provide in our Github below a functional chrome extension frontend. When first loading the extension, a form will be provided to determine the user's dietary preferences. On Amazon.com, we gather the proper keywords and send it to our Transposit backend. This backend interfaces with our DymaboDB database for persistent user data. We also created our model for determining if the product has certain ingredients based on keywords. This backend data is sent back to our frontend. Later we would implement a display of green or red if a product is right for our user.
